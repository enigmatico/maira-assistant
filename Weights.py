import re

class Weights():
    # Weights that dictate the value of specific wildcard characters.
    # The following wildcard characters are available:
    # *: MATCH ANY OR NO WORDS (2 points)
    # _: MATCH ONLY ONE WORD (4 points)
    # ?: MATCH ONE OR NO WORDS (6 points)
    # ^: MATCH AT LEAST ONE WORD (1 point)
    WEIGHTS = {
            "*": 2,
            "_": 4,
            "?": 6,
            "^": 1
    }
    
    # Replace pattern willcards with their corresponding regexp representation.
    @staticmethod
    def preprocess(msg: str) -> str:
        operators = {
            " ":"\s",
            "*":"(.+)",
            "_":"(\\w+)",
            "?":"?(\\w+)?",
            "^":"?(.*)?"
        }
        temp = msg.lstrip().upper()
        for op in operators.keys():
            temp = temp.replace(op, operators[op])
        temp = re.sub("^\?+", "", temp)
        temp = "^" + temp.replace("\\s\\s", "\\s") + "$"
        return temp
    
    @staticmethod
    def calculate_weight(pattern: str) -> int:
        weight = 0
        words = pattern.split(' ')
        for w in words:
            if w in Weights.WEIGHTS.keys():
                weight += Weights.WEIGHTS[w]
            else:
                weight += 10 # We add 10 points per exact match
        return weight