from Core import ChatbotCore
import asyncio

async def main():
    bot = ChatbotCore()

    inp = ""
    while inp.lower() not in ("quit", "exit"):
        inp = input("User> ")
        #resp = await bot.compute_response(inp)
        print(f"Bot> {await bot.compute_response(inp)}")

if __name__ == "__main__":
    asyncio.run(main())
