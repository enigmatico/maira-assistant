import os
import random
import json
import re
import xml.etree.ElementTree as ET
import asyncio
from Model import Model
from Weights import Weights

class ChatbotCore():
    # The 'models' dict works as follows:
    # models[<first character dict>][<second character list>] = Model
    models = {"*":[],"_":[],"?":[],"^":[]}

    def __init__(self):
        for filename in os.listdir('chatbot'):
            if filename.endswith(".pyb"):
                print("Processing %s..." % filename)
                self.__parse_concept("chatbot/" + filename)

    # Parse a pyb file and make a Model list (stored in self.models)
    def __parse_concept(self, fname):
        fhand = open(fname, "r", encoding="utf8")
        self.tree = ET.parse(fhand)
        # First, get the concept object
        self.root = self.tree.getroot().find("Concept")
        # Now find all models in the concept
        for model in self.root.findall('Model'):
            # Create a new model object
            model_obj = Model()
            # Find it's pattern in the model
            pattern = model.find('Pattern')
            if pattern is not None:
                # Compute it's weight:
                model_obj.weight = Weights.calculate_weight(pattern.text)
                # Store the raw pattern text in the model object
                model_obj.raw_pattern = pattern.text
                # Compile the regexp (More memory consumption, but faster)
                model_obj.pattern = re.compile(Weights.preprocess(pattern.text), re.IGNORECASE)
                # Find all the responses
                responses = model.findall('Response')
                for response in responses:
                    # Store all the responses in the model object
                    model_obj.responses.append(response.text)
                # Store the model object in the model list
                # If the first letter dict is not in the models list, add it.
                if pattern.text[0] not in self.models:
                    self.models[pattern.text[0]] = {}
                # If the second letter list is not in the first letter dict, add it.
                if pattern.text[1] not in self.models[pattern.text[0]]:
                    self.models[pattern.text[0]][pattern.text[1]] = []
                # Add the model object
                self.models[pattern.text[0]][pattern.text[1]].append(model_obj)       
        # Close the door when you're done!
        fhand.close()

    def __iterate_model(self, pattern: str, key: str):
        best_match = None
        weight_score = 0
        # Check if there is a matching model pattern. Check for the first character.
        if key in self.models:
            # Check for the second.
            if pattern[1] in self.models[key]:
                # Check for all available models starting with these two characters.
                for (n, model_pattern) in enumerate(self.models[key][pattern[1]]):
                    # Check against the regex pattern
                    if model_pattern.pattern.fullmatch(pattern):
                        # Check if the score is better than our current best:
                        if model_pattern.weight > weight_score:
                            # Set the best match
                            best_match = model_pattern
                            # Update the best score
                            weight_score = model_pattern.weight

        return (best_match, weight_score)
    
    # Compute a response.
    async def compute_response(self, input: str):
        # Best score
        weight_score = 0
        # Best match
        best_match = None
        # Process the input
        input = input.upper()
        
        # Iterates through regular models and wildcards
        for model_key in [input[0], "*", "_", "?", "^"]:
            # Attempts to find the best match
            match, score = self.__iterate_model(input, model_key)
            # Check if we have a better score
            if score > weight_score:
                # Set the best match
                best_match = match
                # Update the best score
                weight_score = weight_score

        # Do we have responses?
        if best_match is not None:
            # From all the available responses, get one. Later this should be the best match.
            return random.choice(best_match.responses)
        else:
            # Default reply
            return "I don't know how to respond to that!"