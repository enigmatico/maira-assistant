import re

class RegexpPatterns():
    match_param = re.compile("(\[:([0-9]+):]+)")
    match_action_param = re.compile("(\[:r([0-9]+):]+)")
    match_bot_param = re.compile("\$\[([A-Za-z0-9]+)\]")
    setvar = re.compile(r"@\[\s([a-zA-Z0-9\s,\.]+)=([a-zA-Z0-9\s,\.]+)\s\]")
    getvar = re.compile(r"@\[\s([a-zA-Z0-9\s,\.]+)\s\]")